<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	// Default values used if batabase value doesn't exist
	public static $defaults =
	[
		'local_disk' =>  'gamma-local',

		'remote_disk' =>  'gamma-ftp',
	];



	public static function get( $name )
	{
		// Get collection
		$setting = Setting::where('name', $name )->orderBy('created_at', 'desc')->get();

		if( is_null( $setting->first() ) )
		{
			return Setting::getDefault( $name );
		}
		else
		{
			return $setting->first()->value;
		}
	}



	public static function getDefault( $name )
	{
		if( isset( Setting::$defaults[ $name ] ) )
		{
			return Setting::$defaults[ $name ];
		}
		else
		{
			abort(500, "Setting '{$name}' not found.");
		}
	}
}
