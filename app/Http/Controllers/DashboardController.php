<?php

namespace App\Http\Controllers;

use App\Dashboard;
use App\File;
use App\Setting;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
	/**
	 * Sync the remote site with the local storage.
	 * 
	 * @return [type] [description]
	 */
	public function sync()
	{
		File::sync();

		File::parse();
	}





	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		echo '<a href="' . action('DashboardController@sync') . '">sync</a>';
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \App\Dashboard  $dashboard
	 * @return \Illuminate\Http\Response
	 */
	public function show(Dashboard $dashboard)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \App\Dashboard  $dashboard
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Dashboard $dashboard)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Dashboard  $dashboard
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Dashboard $dashboard)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  \App\Dashboard  $dashboard
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Dashboard $dashboard)
	{
		//
	}
}
