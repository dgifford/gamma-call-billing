<?php

namespace App;

/**
 * Model for managing Gamma data files
 */

//use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Log;

class File
{
	public $path;

	public $name;

	public $name_parts;

	public $content;



	public function __construct( String $path )
	{
		$this->path = $path;

		$this->name = pathinfo( $path, PATHINFO_FILENAME );

		$this->name_parts = explode( '_', $this->name );

		$this->content = Storage::disk( Setting::get('local_disk') )->get( $path );
	}



	public function getName()
	{
		return pathinfo( $this->path, PATHINFO_FILENAME );
	}



	/////////////////////////////////////
	// Static methods
	/////////////////////////////////////



	/**
	 * Sync the remote site with the local storage.
	 * 
	 * @return [type] [description]
	 */
	public static function sync( $update_all = false )
	{
		$local = Storage::disk( Setting::get('local_disk') )->allFiles('/');

		$remote = Storage::disk( Setting::get('remote_disk') )->allFiles('/');

		if( !$update_all )
		{
			// Exclude remote files already copied
			$remote = array_diff( $remote , $local );
		}

		// Count remaining remote files
		$count = count( $remote );

		Log::info("Starting sync of {$count} remote files to local storage.");

		$info = ['file_count' => 0, 'data_transferred' => 0, ];

		foreach( $remote as $path )
		{
			// Download if forced to update all or if a local copy doesn't exist
			if( $update_all === true or !Storage::disk( Setting::get('local_disk') )->exists( $path ) )
			{
				$size = File::download( $path );
				$info['file_count']++;
				$info['data_transferred']+= $size;
			}
		}

		Log::info("Finished syncing {$info['file_count']} remote files ({$info['data_transferred']} bytes) to local storage." );
	}



	/**
	 * Download a file into local storage.
	 * 
	 * @param  string 	$path The file path relative to the storage root.
	 * @return int 	 	$size 	The size of the file downloaded
	 */
	public static function download( $path )
	{
		Storage::disk( Setting::get('local_disk') )->put(
			$path,
			Storage::disk( Setting::get('remote_disk') )->get( $path )
		);

		return Storage::disk( Setting::get('remote_disk') )->size( $path );
	}



	/**
	 * Parse a file(s) into the DB
	 * 
	 * @return [type] [description]
	 */
	public static function parse( $paths = [] )
	{
		if( !empty($paths) and is_string( $paths ) )
		{
			$paths = [ $paths ];
		}

		if( empty( $paths ) )
		{
			$paths = Storage::disk( Setting::get('local_disk') )->allFiles('/');
		}

		foreach( $paths as $path )
		{
			$file = new File( $path );

			if( $file->isCallData() )
			{
				$file->extractCallData();
			}
		}

	}


	public static function rowToArray( $row = '' )
	{
		$result = explode( ',', $row );

		return array_map(
			function($value) { return trim( $value, '"' ); },
			$result
		);
	}





	/////////////////////////////////////////////////////////////////////
	// Non-static File methods
	/////////////////////////////////////////////////////////////////////



	public function isCallData()
	{
		if( count( $this->name_parts ) > 3 and $this->name_parts[2] == 'Calls' )
		{
			return true;
		}

		return false;
	}



	public function extractCallData()
	{
		$lines = explode( "\n", $this->content );

		// Extract field names
		$cols = File::rowToArray( array_shift( $lines ) );
		$cols = array_map( 'snake_case', $cols );

		foreach( $lines as $num => $line )
		{
			$record = CallRecord::create( array_combine( $cols , File::rowToArray( $line ) ) );
		}
	}
}
