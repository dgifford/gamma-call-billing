<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallRecordsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('call_records', function (Blueprint $table)
		{
			$table->increments('id');

			$table->string('call_type', 4);
			$table->string('call_cause', 1);
			$table->string('customer_identifier', 100);
			$table->string('non-charged_party', 50);
			$table->date('call_date');
			$table->time('call_time');
			$table->integer('duration');
			$table->bigInteger('bytes_transmitted');
			$table->bigInteger('bytes_received');
			$table->string('description', 100);
			$table->string('charge_code', 100);
			$table->string('time_band', 10);
			$table->string('sales_price', 10);
			$table->string('sales_price_pre_bundle', 10);
			$table->string('extension', 6);
			$table->string('ddi', 50);
			$table->string('grouping_id', 100);
			$table->string('call_class', 50);
			$table->string('carrier', 150);
			$table->string('recording', 1);
			$table->string('vat', 1);
			$table->string('country_of_origin', 3);
			$table->string('network', 10);
			$table->string('retail_tariff_code', 8);
			$table->string('remote_network', 50);
			$table->string('apn', 15);
			$table->string('diverted_number', 15);
			$table->string('ring_time', 10);
			$table->string('record_id', 50)->unique();
			$table->string('currency', 3);
			$table->string('caller_line_identity', 30);
			$table->string('network_access_reference', 50);
			$table->string('ngcs_access_charge', 15);
			$table->string('ngcs_service_charge', 15);
			$table->string('total_bytes_transferred', 20);
			$table->string('user_id', 50);
			$table->string('onward_billing_reference', 14);
			$table->string('contract_name', 100);
			$table->string('bundle_name', 100);
			$table->string('bundle_allowance', 50);
			$table->string('discount_reference', 50);
			$table->string('routing_code', 10);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('call_records');
	}
}
